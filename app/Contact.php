<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public function attributes(){
        return $this->hasMany('App\CustomAttribute');
    }
}
