<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\CustomAttribute;
class MapperController extends Controller
{
    public function index(){
        return view('index');
    }

    public function processCSV(Request $request){
        $arrValues = array();
        $arrRow = array();
        $arrCSV = array();
        preg_match_all('/(\,|\r?\n|\r|^)(?:"([^"]*(?:""[^"]*)*)"|([^"\,\r\n]*))/', $request->text, $arrValues);
        if ($arrValues){
            foreach( $arrValues[0] as $key=>$value){
                if ($arrValues[1][$key] && $arrValues[1][$key] !== ','){
                    if ($arrRow){
                        array_push($arrCSV,$arrRow);
                        $arrRow = array();
                    }
                }
                if($arrValues[2][$key]){
                    array_push($arrRow, $arrValues[2][$key] );
                }
                else {
                    array_push($arrRow, $arrValues[3][$key] );
                }
            }
            if ($arrRow){
                array_push($arrCSV,$arrRow);
            }
            $arrCSV=[array_shift($arrCSV), $arrCSV];
        }
        return $arrCSV;
    }

    public function importContacts(Request $request){
        $contacts = $request->contacts;
        $csvFileds = $request->csvfields;
        $mapping = $request->mapping;
        $attributes = $request->attrs;
        $teamidPos = array_search($mapping['team_id'], $csvFileds);
        $namePos = array_search($mapping['name'], $csvFileds);
        $phonePos = array_search($mapping['phone'], $csvFileds);
        $emailPos = array_search($mapping['email'], $csvFileds);
        $stickyPos = array_search($mapping['sticky'], $csvFileds);
        foreach( $contacts as $record){                        
            if($record[$teamidPos] != '' || $record[$phonePos] != '' ){
                $contact = Contact::where('phone', $record[$phonePos])->first();
                if ($contact){
                    $contact->team_id = $record[$teamidPos];
                    $contact->name = $record[$namePos];
                    $contact->phone = $record[$phonePos];
                    $contact->email = $record[$emailPos];
                    $contact->sticky_phone_number_id = $record[$stickyPos];
                    $contact->save();                
                }
                else {
                    $contact = new Contact;
                    $contact->team_id = $record[$teamidPos];
                    $contact->name = $record[$namePos];
                    $contact->phone = $record[$phonePos];
                    $contact->email = $record[$emailPos];
                    $contact->sticky_phone_number_id = $record[$stickyPos];
                    $contact->save();
                }
                CustomAttribute::where('contact_id', $contact->id)->delete();
                if($attributes){
                    $attrs = array();
                    foreach($attributes as $attribute){
                        if( $attribute['key'] != '' && $attribute['field'] != '') {
                            $attr = new CustomAttribute;
                            $attr->timestamps = false;
                            $attr->contact_id = $contact->id;
                            $attr->key = $attribute['key']; 
                            $attr->value = $record[array_search($attribute['field'], $csvFileds)];
                            array_push($attrs, $attr);
                        }
                    }
                    $contact->attributes()->saveMany($attrs);
                }
            }
        }
    }

}
