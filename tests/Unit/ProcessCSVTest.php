<?php

namespace Tests\Unit;

use Tests\TestCase;

class ProcessCSVTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIndex(){
	    $response = $this->get('/');
	    $response->assertStatus(200);

    }

    public function testProcessCsv(){ 
        //Loading contacts file
        $text = file_get_contents('./11-contacts.csv');
        $response = $this->post('/api/processcsv', ['text'=> $text]);
        $csvFile = $response->decodeResponseJson();
        //testing we get and array with valid indexes
        $this->assertArrayHasKey('0', $csvFile);
        $this->assertArrayHasKey('1', $csvFile);
        //testing we get some values on those indexes
        $this->assertTrue(boolval($csvFile[0][0]));
        $this->assertArrayHasKey('0',$csvFile[1]);
    }

    public function testProcessEmptyCsv(){ 
        $response = $this->post('/api/processcsv', ['text'=> '']);
        $csvFile = $response->decodeResponseJson();
        //Testing we get an array with two indexes
        $this->assertArrayHasKey('0', $csvFile);
        $this->assertArrayHasKey('1', $csvFile);
        //Testing we have empty values in the array
        $this->assertFalse(boolval($csvFile[0][0]));
        $this->assertArrayNotHasKey('0',$csvFile[1]);
    }
}
