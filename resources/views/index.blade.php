@extends('layouts.mapper')

@section('content')
    <form method="post" action="/mapfields">
        @csrf
        <csvmapper></csvmapper>
    </form>
@endsection
